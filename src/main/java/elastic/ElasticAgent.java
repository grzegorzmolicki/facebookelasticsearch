package elastic;

import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;

import java.io.IOException;
import java.util.List;

public class ElasticAgent {

	private RestHighLevelClient elasticSearchClient;

	public ElasticAgent() {
		elasticSearchClient = new RestHighLevelClient(
				RestClient.builder(
						new HttpHost("localhost", 9200, "http"),
						new HttpHost("localhost", 9201, "http")));
	}

	public void index(String jsonContent) {
		IndexRequest indexRequest = new IndexRequest("messagesParsed", XContentType.JSON.mediaType());
		indexRequest.source(jsonContent, XContentType.JSON);
		try {
			this.elasticSearchClient.index(indexRequest);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void index(List<String> jsonContent) {
		BulkRequest bulkRequest = new BulkRequest();
		jsonContent.forEach(message ->
			bulkRequest.add(new IndexRequest("messages", XContentType.JSON.mediaType()).source(message, XContentType.JSON))
						   );
		try {
			this.elasticSearchClient.bulk(bulkRequest);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void close() {
		try {
			this.elasticSearchClient.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
