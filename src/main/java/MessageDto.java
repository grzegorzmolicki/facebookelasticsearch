public class MessageDto {

	private String timestamp;
	private String author;
	private String content;
	private String conversationWith;

	MessageDto(String timestamp, String author, String content, String conversationWith) {
		this.timestamp = timestamp;
		this.author = author;
		this.content = content;
		this.conversationWith = conversationWith;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public MessageDto setTimestamp(String timestamp) {
		this.timestamp = timestamp;
		return this;
	}

	public String getAuthor() {
		return author;
	}

	public MessageDto setAuthor(String author) {
		this.author = author;
		return this;
	}

	public String getContent() {
		return content;
	}

	public MessageDto setContent(String content) {
		this.content = content;
		return this;
	}

	public String getConversationWith() {
		return conversationWith;
	}

	public MessageDto setConversationWith(String conversationWith) {
		this.conversationWith = conversationWith;
		return this;
	}
}
