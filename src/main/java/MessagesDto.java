import java.util.List;

public class MessagesDto {

	private List<MessageDto> messages;

	public MessagesDto(List<MessageDto> messages) {
		this.messages = messages;
	}

	public List<MessageDto> getMessages() {
		return messages;
	}

	public MessagesDto setMessages(List<MessageDto> messages) {
		this.messages = messages;
		return this;
	}
}
