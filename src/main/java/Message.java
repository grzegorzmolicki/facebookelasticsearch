public class Message {
	private String time;
	private String author;
	private String content;
	private String conversation;

	public Message(String time, String author, String content, String conversation) {
		this.time = time;
		this.author = author;
		this.content = content;
		this.conversation = conversation;
	}

	public String getTime() {
		return time;
	}

	public Message setTime(String time) {
		this.time = time;
		return this;
	}

	public String getAuthor() {
		return author;
	}

	public Message setAuthor(String author) {
		this.author = author;
		return this;
	}

	public String getContent() {
		return content;
	}

	public Message setContent(String content) {
		this.content = content;
		return this;
	}

	public String getConversation() {
		return conversation;
	}

	public Message setConversation(String conversation) {
		this.conversation = conversation;
		return this;
	}
}
