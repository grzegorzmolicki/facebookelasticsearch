import java.util.ArrayList;
import java.util.List;

class Conversation {
	private String        with;
	private List<Message> messages;

	Conversation(String with) {
		this.with = with;
		messages = new ArrayList<>();
	}

	public void addMessage(Message message) {
		this.messages.add(message);
	}

	public String getWith() {
		return with;
	}

	public Conversation setWith(String with) {
		this.with = with;
		return this;
	}

	public List<Message> getMessages() {
		return messages;
	}

	public Conversation setMessages(List<Message> messages) {
		this.messages = messages;
		return this;
	}
}
