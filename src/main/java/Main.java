import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import elastic.ElasticAgent;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.stream.Collectors;

public class Main {

	private static final int MONTH_IDX             = 1;
	private static final int DAY_OF_MONTH_IDX      = 2;
	private static final int YEAR_IDX              = 3;
	private static final int HOUR_WITH_MINUTES_IDX = 5;
	private static final int ZONE_IDX              = 6;

	private static final String FB_USER = "Facebook User";

	private static String getField(String[] fbDateTimeSplit, int fieldIdx) {
		return fbDateTimeSplit[fieldIdx].replaceAll(",", "").trim();
	}

	private static LocalDateTime parseDateToNonRetardedFormat(String fbLocationDateTime) {
		String[] time = fbLocationDateTime.split(" ");
		String dayString = getField(time, DAY_OF_MONTH_IDX);
		String monthString = getField(time, MONTH_IDX).substring(0, 3);
		String yearString = getField(time, YEAR_IDX);
		String hourWithMinutesString = getField(time, HOUR_WITH_MINUTES_IDX).toUpperCase();
		String zone = getField(time, ZONE_IDX);

		if (hourWithMinutesString.indexOf(":") == 1) {
			hourWithMinutesString = "0" + hourWithMinutesString;
		}

		if (dayString.length() == 1) {
			dayString = "0" + dayString;
		}

		LocalDateTime localDateTime = LocalDateTime.from(
				DateTimeFormatter.ofPattern("d MMM yyyy hh:mma", Locale.ENGLISH)
								 .parse(dayString + " " + monthString + " " + yearString + " " + hourWithMinutesString)
												 );
		localDateTime.atZone(ZoneId.of(zone));

		return localDateTime;

	}

	public static void main(String... args) throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		ElasticAgent elasticAgent = new ElasticAgent();

		long messagesCounter = 0L;
		long start = System.currentTimeMillis();

		for(int i = 0; i < 426; i++) {
			File f = new File("C:\\Users\\Grzegorz\\Documents\\fbdump\\messages\\"+i+".html");
			try {
				Document doc = Jsoup.parse(f, "UTF-8");
				Elements conversationElements = doc.getElementsByClass("message");
				String conversationWith = doc.getElementsByTag("h3").first().text().replace("Conversation with ", "").trim();

				if(conversationWith.equals(FB_USER)) {
					conversationWith = ""+i;
				}
				Conversation conversation = new Conversation(conversationWith);

				for (Element element : conversationElements) {

					String message = element.nextElementSibling().text();
					String author = element.children().select("span").get(0).text(); // author
					String rawTime = element.children().select("span").get(1).text();
					LocalDateTime messageSentTime = parseDateToNonRetardedFormat(rawTime);

					Message msg = new Message(messageSentTime.format(DateTimeFormatter.ISO_LOCAL_DATE), author, message, conversationWith);
					conversation.addMessage(msg);
				}


				messagesCounter += conversation.getMessages().size();

				MessagesDto messagesDto = new MessagesDto(conversation.getMessages().stream()
							.map(message -> new MessageDto(message.getTime(), message.getAuthor(), message.getContent(), message.getConversation()))
							.collect(Collectors.toList()));


				elasticAgent.index(messagesDto.getMessages().stream().map(value -> {
					try {
						return objectMapper.writeValueAsString(value);
					} catch (JsonProcessingException e) {
						e.printStackTrace();
					}
					return "";
				}).collect(Collectors.toList()));
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		long diff = System.currentTimeMillis() - start;
		System.out.println();
		System.out.println("Messages: " + messagesCounter);
		System.out.println("Calc took: " + diff);
		elasticAgent.close();
	}
}
