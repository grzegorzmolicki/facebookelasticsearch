# SIP

##### v1.1 CHANGESET
- Split screen is now forbidden
- Recorded data is saved as valid GPX file
- Toast message about being logged in using stored credentials is now shown properly
